import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS = [
    {label:'Nome',name: 'nome'}
]

const FUNCOESOOPT = [
    {label: '', value : ''},
    {label: 'teste', value : '2'},
    {label: 'tedste', value: '3'}
]

const INSERIR  = [
    {label: 'Ordem',                     id: 'VeiculoOrdemInput',              required: true,  name: 'nome', cols: "12", type: "txt", component: "input" },
    {label: 'Placa',                     id: 'VeiculoPlacaInput',              required: true,  name: '', cols: "12", type: "txt", component: "input"},
    {label: 'Tipo',                      id: 'VeiculoTipoInput',               required: true,  name: '', cols: "12", type: "txt", component: "select",  options: FUNCOESOOPT},
    {label: 'Empresa',                   id: 'VeiculoEmpresaInput',            required: true,  name: '', cols: "12", type: "txt", component: "select", options: FUNCOESOOPT},
    {label: 'Fornecedor do DVR ',        id: 'VeiculoFornecedorDVR',           required: true,  name: '', cols: "12", type: "txt", component: "select", options: FUNCOESOOPT},
    {label: 'IP do DVR ',                id: 'VeiculoIdDvrInput',              required: true,  name: '', cols: "12", type: "txt", component: "input"},
    {label: 'Início atividade',          id: 'VeiculoInicioAtividadeInput',    required: true,  name: '', cols: "12", type: "date",component: "input"},
    {label: 'Fim atividade',             id: 'VeiculoFimAtividadeInput',       required: false, name: '', cols: "12", type: "date",component: "input"},
    {label: 'Schema API de Itinerários', id: 'VeiculoSchemaApiItinerarioInput',required: false, name: '', cols: "12", type: "txt", component: "input"},
]

class Perfis extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Perfil"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  listRequest   ='perfis'
                  createTipo    ='perfl'
                  sorting       = {true}
                  colunas       ={COLUNAS}
                  camposInserir ={INSERIR}
            />
        )
    }
}

export default Perfis;
