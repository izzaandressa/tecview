import React, { Component } from 'react'
import ContentHeader        from '../common/template/contentHeader'
import Content              from '../common/template/content'

class Configuracao extends Component {
    render() {
        return (
            <div>
                <ContentHeader title={this.props.title} small={this.props.small} />
                <Content>

                </Content>
            </div>
        )
    }
}

export default Configuracao;
