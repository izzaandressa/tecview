import React, { Component } from 'react'

import CRUD                 from '../common/crud0/crud'

const COLUNAS = [
    {label:'Nome', name: 'nome'}
]

const INSERIR = [
    {label: 'Nome',               id: 'EmpresaNomeInput'        , required: true,  name: 'nome',                 cols: "12", type: "txt", component: "input" },
    {label: 'API GPS'           , id: 'EmpresaApiItinerarionput', required: false, name: 'schema_api_itinerario',cols: "12", type: "txt", component: "input" },
    {label: 'API Validador'     , id: 'UrlApiauttranInput'      , required: false, name: 'url_api_auttran',      cols: "12", type: "txt", component: "input" },
    {label: 'Login Validador'   , id: 'LoginValidador'          , required: false, name: 'login_validador',      cols: "12", type: "txt", component: "input" },
    {label: 'Password Validador', id: 'PasswordValidador'       , required: false, name: 'password_validador',   cols: "12", type: "txt", component: "input" },
]

class Empresas extends Component {
    render() {
        return (
            <CRUD title         = "Cadastro de Empresa"
                  small         = "Listagem"
                  colunas       = {COLUNAS}
                  listRequest   = 'empresas'
                  sorting       = {true}
                  createTipo    ='empresa'
                  camposInserir = {INSERIR}
            />
        )
    }
}

export default Empresas;
