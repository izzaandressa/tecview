import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS = [
    {label:'Nome',  name: 'nome'},
    {label:'Login', name: 'login'},
    {label:'E-mail',name: 'email'},
    {label:'Perfil',name: 'nome_perfil'},
]

const INSERIR  = [
    {label: 'Nome',                          id: 'UsuarioNomeInput',          required: true,  name: 'nome',              cols: "12", type: "txt",     component: "input" },
    {label: 'Sobrenome',                     id: 'UsuarioSobrenomeInput',     required: true,  name: 'sobrenome',         cols: "12", type: "txt",     component: "input" },
    {label: 'Login',                         id: 'LoginInput',                required: true,  name: 'login',             cols: "12", type: "txt",     component: "input" },
    {label: 'E-mail',                        id: 'UsuarioEmailInput',         required: true,  name: 'email',             cols: "12", type: "email",   component: "input" },
    {label: 'Perfil',                        id: 'UsuarioPerfilInput',        required: true,  name: 'perfil_id',         cols: "12", type: "txt",     component: "input" },
    {label: 'Empresa',                       id: 'UsuarioEmpresaInput',       required: true,  name: 'empresasSelecionadas', cols: "12", type: "txt",     component: "input" },
    {label: 'Senha * (minimo 8 caracteres)', id: 'UsuarioSenhaInput',         required: true,  name: 'password',          cols: "12", type: "password",component: "input" },
    {label: 'Confirme a senha',              id: 'UsuarioSenhaConfirmaInput', required: true,  name: 'password_confirma', cols: "12", type: "password",component: "input" },
]

class Usuarios extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Usuários"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  listRequest   ='usuarios'
                  createTipo    ='usuario'
                  sorting       = {true}
                  colunas       ={COLUNAS}
                  camposInserir ={INSERIR}
            />
        )
    }
}

export default Usuarios;
