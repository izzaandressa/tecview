import { combineReducers }          from 'redux'
import { reducer as formReducer }   from 'redux-form'
import { reducer as toastrReducer } from 'react-redux-toastr'

import TabReducer     from '../common/tab/tabReducer'
import CrudReducer    from '../common/crud/crudReducer'
import AnaliseReducer from '../monitoramento/analise/analiseReducer'

const rootReducer = combineReducers({
    tab         : TabReducer,
    form        : formReducer,
    toastr      : toastrReducer,
    crud        : CrudReducer,
    analise     : AnaliseReducer
})

export default rootReducer
