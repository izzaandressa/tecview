import React                                                from 'react'
import { Router, Route, IndexRoute, Redirect, hashHistory } from 'react-router'

import App                  from './app'

import Home                 from '../home/home'
//Configuracao
import BancoDeImagens       from '../configuracao/bancoDeImagens'
import ConfiguracaoSistema  from '../configuracao/configuracaoSistema'
import Perfis               from '../configuracao/perfis'
import Usuarios             from '../configuracao/usuarios'
// Corporativo
import Empresas             from '../corporativo/empresas'
import Funcionarios         from '../corporativo/funcionarios'
import Funcoes              from '../corporativo/funcoes'
// Monitoramento
import Analises             from '../monitoramento/analises'
import Grupos               from '../monitoramento/grupos'
import Listagens            from '../monitoramento/listagens'
import Ocorrencias          from '../monitoramento/ocorrencias'
import Relatorios           from '../monitoramento/relatorios'
import Tipos                from '../monitoramento/tipos'
// Operacioonal
import Linhas               from '../operacional/linhas'
import Setores              from '../operacional/setores'
import Veiculos             from '../operacional/veiculos'
//Wifi
import Antenas              from '../wifi/antenas'
import Coleta               from '../wifi/coleta'
import RelatoriosWifi       from '../wifi/relatorios'

export default props => (
    <Router history={hashHistory}>
        <Route path='/' component={App}>
            <IndexRoute                       component={Home}                />

            <Route path='bancoDeImagens'      component={BancoDeImagens      } />
            <Route path='configuracaoSistema' component={ConfiguracaoSistema } />
            <Route path='perfis'              component={Perfis              } />
            <Route path='usuarios'            component={Usuarios            } />

            <Route path='empresas'            component={Empresas            } />
            <Route path='funcionarios'        component={Funcionarios        } />
            <Route path='funcoes'             component={Funcoes             } />

            <Route path='analises'            component={Analises            } />
            <Route path='grupos'              component={Grupos              } />
            <Route path='listagens'           component={Listagens           } />
            <Route path='ocorrencias'         component={Ocorrencias         } />
            <Route path='relatorios'          component={Relatorios          } />
            <Route path='tipos'               component={Tipos               } />

            <Route path='linhas'              component={Linhas              } />
            <Route path='setores'             component={Setores             } />
            <Route path='veiculos'            component={Veiculos            } />

            <Route path='antenas'             component={Antenas             } />
            <Route path='coleta'              component={Coleta              } />
            <Route path='relatoriosWifi'      component={RelatoriosWifi      } />
        </Route>
        <Redirect from='*' to='/' />
    </Router>
)
