import '../common/template/dependencies'
import React from 'react'

import Header   from '../common/template/header'
import SideBar  from '../common/template/sideBar'
import Footer   from '../common/template/footer'
import Messages from '../common/msg/messages'
import Favicon  from 'react-favicon'
import favicon  from '../img/logo_32x32.png'


export default props => (
    <div className='wrapper'>
        <Favicon url={favicon}/>
        <Header  />
        <SideBar />
        <div className='content-wrapper'>
            {props.children}
        </div>
        {/*<Footer   />*/}
        <Messages />
    </div>
)
