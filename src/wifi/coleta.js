import React, { Component } from 'react'
import ContentHeader        from '../common/template/contentHeader'
import Content              from '../common/template/content'

class Coleta extends Component {
    render() {
        return (
            <div>
                <ContentHeader title={'Coleta Wifi '} small={'Listagem'} />
                <Content>

                </Content>
            </div>
        )
    }
}

export default Coleta;
