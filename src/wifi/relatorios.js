import React, { Component } from 'react'
import ContentHeader        from '../common/template/contentHeader'
import Content              from '../common/template/content'

class Relatorio extends Component {
    render() {
        return (
            <div>
                <ContentHeader title={'Relatório '} small={'Antena wifi'} />
                <Content>

                </Content>
            </div>
        )
    }
}

export default Relatorio;
