import React, { Component } from 'react'
import LogoCompleta         from '../img/logo.png'
import Content              from '../common/template/content'
import Particles            from 'react-particles-js';
import {config}             from './particlesConf';
import './home.css'

class Home extends Component {
    render() {
        return (
            <Content className="content-home">
                <Particles params={config} />
                <div className="jumbotron jumbotron-fluid">
                    <div className="home-container">
                        <img className='home-logo' src={LogoCompleta}/>
                        <br />
                        <br />
                        <p className="lead"><b>TECVIEW</b>©. Um produto Tecno Mobile. Todos os direitos reservados. | Release: <b>develop</b> Build</p>
                    </div>
                </div>
           </Content>
        )
    }
}

export default Home;
