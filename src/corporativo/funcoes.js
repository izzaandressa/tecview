import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS = [
    {label:'Nome', name: 'nome'}
]

const INSERIR     = [
    {label: 'Nome',   id: 'FuncaoNomeInput',   required: true, name: 'nome',   cols: "12", type: "txt", component: "input" },
    {label: 'Código', id: 'FuncaoCodigoInput', required: true, name: 'codigo', cols: "12", type: "txt", component: "input" },
]

class Funcoes extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Funções"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  sorting       = {true}
                  listRequest   ='funcoes'
                  createTipo    ='funcao'
                  camposInserir ={INSERIR}
            />
        )
    }
}

export default Funcoes;
