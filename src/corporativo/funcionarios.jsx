import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS = [
    {label:'Nome',      name: 'nome'        },
    {label:'Matricula', name: 'matricula'   },
    {label:'Função',    name: 'nome_funcao' },
    {label:'Empresa',   name: 'nome_empresa'}
]

const EMPRESASOPT = [
    {label: '',      value : ''},
    {label: 'teste', value : '2'},
    {label: 'tedste',value: '3'}
]

const FUNCOESSOPT = [
    {label: '',      value : ''},
    {label: 'teste', value : '2'},
    {label: 'tedste',value: '3'}
]

const INSERIR     = [
    {label: 'Nome',      id: 'FuncionarioNomeInput',      required: true, name: 'nome',      cols: "12", type: "txt", component: "input" },
    {label: 'Matrícula', id: 'FuncionarioMatriculaInput', required: true, name: 'matricula', cols: "12", type: "txt", component: "input" },
    {label: 'Fuções',    id: 'FuncionarioFuncao',         required: true, name: 'funcao_id', cols: "12", type: "txt", component: "select", options: EMPRESASOPT },
    {label: 'Empresa',   id: 'FuncionarioEmpresa',        required: true, name: 'empresa_id',cols: "12", type: "txt", component: "select", options: FUNCOESSOPT },
]

class Funcionarios extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Funcionário"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  sorting       = {true}
                  listRequest   ='funcionarios'
                  createTipo    ='funcionario'
                  camposInserir ={INSERIR}
            />
        )
    }
}

export default Funcionarios;
