import React, { Component } from 'react'

import CRUD                 from '../common/crud0/crud'

const COLUNAS = [
    {label:'Nome', name: 'nome'}
]

const INSERIR = [
    {label: 'Nome',               id: 'EmpresaNomeInput'        , required: true,  name: 'nome',                 cols: "12", type: "txt", component: "input" },
    {label: 'API GPS'           , id: 'EmpresaApiItinerarionput', required: false, name: 'schema_api_itinerario',cols: "12", type: "txt", component: "input" },
    {label: 'API Validador'     , id: 'UrlApiauttranInput'      , required: false, name: 'url_api_auttran',      cols: "12", type: "txt", component: "input" },
    {label: 'Login Validador'   , id: 'LoginValidador'          , required: false, name: 'login_validador',      cols: "12", type: "txt", component: "input" },
    {label: 'Password Validador', id: 'PasswordValidador'       , required: false, name: 'password_validador',   cols: "12", type: "txt", component: "input" },
]

const ACTIONS = [
    {action:'showUpdate' , sorting: true, fa: 'pencil'},
    {action:'showDelete' , sorting: true, fa: 'trash-o'}
]

class Empresas extends Component {
    render() {
        return (
            <CRUD title         = "Cadastro de Empresa"
                  small         = "Listagem"
                  listRequest   = 'empresas'
                  createTipo    ='empresa'
                  colCards      = 'col-md-4 col-sm-12'
                  sorting       = {true   }
                  colunas       = {COLUNAS}
                  camposInserir = {INSERIR}
                  actions       = {ACTIONS}
            />
        )
    }
}

export default Empresas;
