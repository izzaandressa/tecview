import React from 'react'

export default props => (
    <li className='treeview'>
        <a href={props.path} className={props.label == 'Usuário' ? 'sidebarUser' : ''}>
            <i className={`fa fa-${props.icon}`}></i> <span>{props.label}</span>
            {
                props.iconOff ? <i className='fa fa-power-off pull-right'></i> : <i className='fa fa-angle-left pull-right'></i>
            }
        </a>
        <ul className='treeview-menu'>
            {props.children}
        </ul>
    </li>
)
