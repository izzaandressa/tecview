import React, { Component }   from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'
import {showUpdate, showDelete} from './actions'

import './inBox.css'

class Box extends Component {

    actionsBox(actions, elemen, sorting = {sorting: {}}){
        return actions.map((item, key)=>(
             <button key={`action-${key}`} className="btn  btn-sm btn-danger actions-table" onClick={() => this.props[item.action](elemen, sorting)}>
                <i className={`fa fa-${item.fa} action-${item.fa}`}></i>
             </button>
         ))
    }

    contentBox(element,conten){
        return conten.map((item, key)=>(
            <li key={`${key}-content-list`}>
                <span><b>{item.label}: </b></span>
                <span>{element[item.name]}</span>
            </li>
         ))
    }

    render() {
        const {actions, conten, title, status, col, elemen, sorting} = this.props
        return (
            <section className={col}>
                <div className="ibox">
                    <div className="ibox-title">
                        {
                            status ? <span className={`label label-${status}`}>{status}</span> : null
                        }
                        <h5>{title}</h5>
                        <div className="ibox-tools">
                            {this.actionsBox(actions, elemen)}
                        </div>
                    </div>
                    <div className="ibox-content" >
                        <ul className="list-group list-group-flush">
                            {this.contentBox(elemen, conten, sorting)}
                        </ul>
                    </div>
                </div>
            </section>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({showUpdate, showDelete}, dispatch)
export default connect(null, mapDispatchToProps)(Box)
