import axios                                             from 'axios'
import { reset as resetForm, initialize, initialValues } from 'redux-form'
import { toastr }                                        from 'react-redux-toastr'

import { showTabs, selectTab }  from '../../tab/tabActions'
import '../../crud0/actions'

export function showUpdate(crud) {
    return [
        showTabs('tabUpdate'),
        selectTab('tabUpdate'),
        initialize('crudForm', crud)
    ]
}

export function showDelete(crud) {
    return [
        showTabs('tabDelete'),
        selectTab('tabDelete'),
        initialize('crudForm', crud)
    ]
}


export function backList(tipo, sorting){
    return [
        init(tipo, sorting)
    ]
}
