import React         from 'react'
import { Link }      from 'react-router'
import LogoCompleta  from '../../img/logo.png'
import LogoCollapsed from '../../img/logo-collapsed.png'

export default props => (
    <header className='main-header'>
        <Link to="/" className='logo'>
            <img className='logo-mini logoCustom' src={LogoCollapsed}/>
            <img className='logo-lg' src={LogoCompleta}/>
        </Link>
        <nav className='navbar navbar-static-top'>
            <a href className='sidebar-toggle' data-toggle='offcanvas'></a>
        </nav>
    </header>
)
