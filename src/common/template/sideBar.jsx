import React    from 'react'
import Menu     from './menu'
import MenuTree from './menuTree'

export default props => (
    <aside className='main-sidebar'>
        <section className='sidebar'>
            <Menu />
        </section>
    </aside>
)
