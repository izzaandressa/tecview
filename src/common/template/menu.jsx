import React    from 'react'
import MenuItem from './menuItem'
import MenuTree from './menuTree'

export default props => (
    <ul className='sidebar-menu'>
        <MenuTree label='Usuário'    icon="user" iconOff='power-off'/>
        <MenuTree label='Corporativo'    icon='briefcase'>
            <MenuItem path='empresas'            label='Empresas'         />
            <MenuItem path='Funcionarios'        label='Funcionários'     />
            <MenuItem path='funcoes'             label='Funções'          />
        </MenuTree>
        <MenuTree label='Monitoramento' icon='desktop'>
            <MenuItem path='analises'            label='Análises'         />
            <MenuItem path='listagens'           label='Listagens'        />
            <MenuItem path='grupos'              label='Grupos'           />
            <MenuItem path='tipos'               label='Tipos'            />
            <MenuItem path='ocorrencias'         label='Ocorrências'      />
            <MenuItem path='relatorios'          label='Relatórios'       />
        </MenuTree>
        <MenuTree label='Operacional'   icon='bus'>
            <MenuItem path='veiculos'            label='Veículos'         />
            <MenuItem path='setores'             label='Setores'          />
            <MenuItem path='linhas'              label='Linhas'           />
        </MenuTree>
        <MenuTree label='Wi-Fi'         icon='wifi'>
            <MenuItem path='antenas'             label='Antenas'          />
            <MenuItem path='coleta'              label='Coleta'           />
            <MenuItem path='relatoriosWifi'      label='Relatórios'       />
        </MenuTree>
        <MenuTree label='Configurações' icon='cog'>
            <MenuItem path='configuracaoSistema' label='Geral do Sistema' />
            <MenuItem path='bancoDeImagens'      label='Banco de Imagens' />
            <MenuItem path='usuarios'            label='Usuários'         />
            <MenuItem path='perfis'              label='Perfis'           />
        </MenuTree>
    </ul>
)
