import axios             from 'axios'
import {BASE_URL, TOKEN} from './configRequest'


export function getDropDown(tipo) {
    const request = axios.get(`${BASE_URL}/dropdown/${tipo}`, {
       'headers': {
          'Content-Type' : 'application/json',
          'Authorization': "Bearer " + TOKEN ,
        }
    })
    return request
}
