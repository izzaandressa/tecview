/* Esses métodos servem para efetuar requisições de crud do sistema*/
import {BASE_URL, HEADERS} from './configRequest'
import { toastr }          from 'react-redux-toastr'
import axios               from 'axios'

/*
* @param string method
* @param array values
*/
export function submit(tipo, tipoList, data, method) {
    return dispatch => {
        axios[method](`${BASE_URL}/${tipo}`, data, HEADERS)
            .then(resp => {
                console.log(tipoList);
                toastr.success('Sucesso', 'Operação Realizada com sucesso.')
                dispatch(init(tipoList))
            })
            .catch(e => {
                //toastr.error('Erro', 'Operação não realizada.')
                e.response.data.errors.forEach(error => toastr.error('Erro', error))
            })
    }
}

//const DATA = {"sorting":{"nome":"asc"}}
export function getListRequest(tipo, data) {
    //const request = axios.post(`${BASE_URL}/v1/${tipo}?page=1`, DATA, {
    const request = axios.post(`${BASE_URL}/${tipo}?page=1`,  data , HEADERS)
    return request
}
