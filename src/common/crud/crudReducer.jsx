const INITIAL_STATE = {list: []}

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case 'CRUD_FETCHED':
            return { ...state, list: action.payload.data ? action.payload.data.data : [] }
        default:
            return state
    }
}
