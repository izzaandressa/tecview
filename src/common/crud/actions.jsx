import axios                                             from 'axios'
import { reset as resetForm, initialize, initialValues } from 'redux-form'
import { toastr }                                        from 'react-redux-toastr'

import { showTabs, selectTab }  from '../tab/tabActions'
import { getListRequest }       from '../request/crudRequest'
import {BASE_URL, HEADERS}      from '../request/configRequest'

const INITIAL_VALUES = {}

export function submit(values) {
    let data = values.data || ''
    let id   = values.id   || ''
    return dispatch => {
        axios[values.method](`${BASE_URL}/${values.tipo}/${id}`, data, HEADERS)
            .then(resp => {
                toastr.success('Sucesso', 'Operação Realizada com sucesso.')
                dispatch(init(values.tipoList, values.sorting ))
            })
            .catch(e => {
                e.response.data.errors.forEach(error => toastr.error('Erro', error))
            })
    }
}

export function Delete(values) {
    return dispatch => {
        axios.delete(`${BASE_URL}/${values.tipo}/${values.id}`, HEADERS)
            .then(resp => {
                toastr.success('Sucesso', 'Operação Realizada com sucesso.')
                dispatch(init(values.tipoList, values.sorting))
            })
            .catch(e => {
                e.response.data.errors.forEach(error => toastr.error('Erro', error))
            })
    }
}

export function getList(tipo, sorting) {
    let data = sorting ? {"sorting":{nome: "asc"}} : {sorting: {}}
    return {
        type     : 'CRUD_FETCHED',
        payload  : getListRequest(tipo, data)
    }
}

export function resetList() {
    return {
        type   : 'CRUD_FETCHED',
        payload: ''
    }
}

export function create(tipo, tipoList, item) {
    let value = {
        tipo     : tipo,
        tipoList : tipoList,
        data     : item,
        sorting  : item.sorting,
        method   : 'post'
    }

    return submit(value)
}

export function update(tipo, tipoList, item) {
    let data = {
        tipo     : tipo,
        tipoList : tipoList,
        id       : item.id,
        data     : item,
        sorting  : item.sorting,
        method   : 'put'
    }

    return submit(data)
}

export function show(tipo, tipoList, item) {
    let data = {
        tipo     : tipo,
        tipoList : tipoList,
        id       : item.id,
        sorting  : item.sorting,
        method   : 'get'
    }

    return submit(data)
}

export function remove(tipo, tipoList, item) {
    let data = {
        tipo     : tipo,
        tipoList : tipoList,
        id       : item.id,
        sorting  : item.sorting,
    }

    return Delete(data)
}

export function showUpdate(crud) {
    return [
        showTabs('tabUpdate'),
        selectTab('tabUpdate'),
        initialize('crudForm', crud)
    ]
}

export function showDelete(crud) {
    return [
        showTabs('tabDelete'),
        selectTab('tabDelete'),
        initialize('crudForm', crud)
    ]
}

export function toShow(crud) {
    return [
        showTabs('tabShow'),
        selectTab('tabShow'),
        initialize('crudForm',crud)
    ]
}

export function showCreate(crud) {
    return [
        showTabs('tabCreate'),
        selectTab('tabCreate'),
        initialize('crudForm', INITIAL_VALUES)
    ]
}

export function init(tipo, sorting) {
    return [
        resetList(),
        showTabs('tabList', 'tabCreate'),
        selectTab('tabList'),
        getList(tipo, sorting),
        initialize('crudForm', INITIAL_VALUES)
    ]
}
