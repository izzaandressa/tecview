import React, { Component }   from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'
import { getList, showUpdate, showDelete, toShow } from './actions'

import Loading from 'react-loading';

class Tabela extends Component {
    componentWillMount() {
        this.props.getList(this.props.listRequest,this.props.sorting)
    }

    renderRows() {
        const list    = this.props.list    || []
        const actions = this.props.actions || [true, true]

        return list.map((item, key1) => (
            <tr key={`${item.id}-tr-${key1}`}>
                {
                    this.props.colunas.map((linha, key) =>  <td onClick={() => this.props.toShow(item, this.props.colunas, this.props.sorting)} key={`${linha.name}-tr-${key}` }>{item[linha.name]}</td>)
                }
                <td>
                    {
                        actions[2] ? <button className='btn  btn-sm btn-danger actions-table pull-right' onClick={() => this.props.showDelete(item, this.props.sorting)}>
                            <i className='fa fa-trash-o action-o'></i>
                        </button> : null
                    }
                    {
                        actions[1] ? <button className='btn btn-sm btn-warning actions-table pull-right' onClick={() => this.props.showUpdate(item, this.props.sorting)}>
                            <i className='fa fa-pencil action-pencil'></i>
                        </button> : null
                    }
                </td>
            </tr>
        ))
    }

    render() {
        const list    = this.props.list || []
        const actions = this.props.actions[0] || this.props.actions[1] ? true : false;
        return (
            <div>
                { list.length ?
                       <table className='table table-striped table-hover cursor-point'>
                        <thead>
                            <tr>
                                {
                                    this.props.colunas.map((coluna,key) => {
                                        return <th key={`${coluna.label}-thead-`}>{coluna['label']}</th>
                                    })

                                }
                                {
                                    actions ? <th className='table-actions actions-title'>Ações</th> : null
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.renderRows()
                            }
                        </tbody>
                    </table> : <div className="div-loading"><Loading  className="loading" type={'bars'} height={40} width={70} color='#222d32' /></div>
                }
            </div>
        )
    }
}

const mapStateToProps    = state    => ({list: state.crud.list})
const mapDispatchToProps = dispatch => bindActionCreators({getList, showUpdate, showDelete, toShow}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Tabela)
