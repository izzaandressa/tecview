import React, { Component }   from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'

import ContentHeader from '../template/contentHeader'
import Content       from '../template/content'
import Tabs          from '../tab/tabs'
import TabsHeader    from '../tab/tabsHeader'
import TabsContent   from '../tab/tabsContent'
import TabHeader     from '../tab/tabHeader'
import TabContent    from '../tab/tabContent'
import {
         init,
         showCreate,
         showUpdate,
         create,
         update,
         remove
     }               from './actions'

import Tabela from './tabela'
import Form   from './form'
import Show   from './show'

class Crud extends Component {
    componentWillMount() {
        this.props.init(this.props.listRequest, this.props.sorting)
    }

    render() {
        const {createTipo, camposInserir, listRequest, sorting} = this.props
        return (
            <div>
                <ContentHeader title={this.props.title} small={this.props.small} />
                <Content>
                    <Tabs>
                        <TabsContent>
                            <TabContent id='tabList'>
                                <button type='button' className='btn btn-primary pull-right'
                                    onClick={() => this.props.showCreate()}>
                                    <i className="fa fa-plus"></i>
                                </button>
                                <button type='button' className='btn btn-primary pull-right' onClick={() => this.props.showUpdate()}>
                                    <i className="fa fa-filter"></i> Filtros
                                </button>
                                <Tabela sorting={sorting} listRequest={listRequest} colunas={this.props.colunas} actions={[true,true,true]}/>
                            </TabContent>
                            <TabContent id='tabCreate'>
                                <Form type="create" sorting={sorting} listRequest={listRequest} onSubmit={(data)=>this.props.create(createTipo,listRequest,data)}
                                 submitLabel='Incluir' submitClass='primary' camposInserir={camposInserir}/>
                            </TabContent>
                            <TabContent id='tabShow'>
                                <Show sorting={sorting} camposInserir={camposInserir}/>
                            </TabContent>
                            <TabContent id='tabUpdate'>
                                <Form type="update" sorting={sorting} listRequest={listRequest} onSubmit={(data)=>this.props.update(createTipo,listRequest,data)}
                                submitLabel='Alterar' submitClass='info' camposInserir={camposInserir} />
                            </TabContent>
                            <TabContent id='tabDelete'>
                                <Form type="delete" sorting={sorting} listRequest={listRequest} onSubmit={(data)=>this.props.remove(createTipo,listRequest,data)} readOnly={true}
                                submitLabel='Excluir' submitClass='danger'camposInserir={camposInserir}  />
                            </TabContent>
                        </TabsContent>
                    </Tabs>
                </Content>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    init, showCreate,showUpdate, create, update, remove
}, dispatch)
export default connect(null, mapDispatchToProps)(Crud)
