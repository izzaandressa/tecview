import React  from 'react'
import Grid   from '../layout/grid'
import MultiSelect from 'multi-select-react';

export default props => (
    <Grid cols={props.cols}>
        <div className='form-group'>
            <label >{props.label}</label>
            <MultiSelect
                 {...props.input}
                 className  ='form-control'
                 placeholder={props.placeholder}
                 readOnly   ={props.readOnly}
                 type       ={props.type}
                 options    ={option.label}
                  isTextWrap={false}
            />
        </div>
    </Grid>
)
