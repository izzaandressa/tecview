import React, { Component }                    from 'react'
import { connect }                             from 'react-redux'
import { bindActionCreators }                  from 'redux'
import { reduxForm, Field, formValueSelector } from 'redux-form'

import { init, load}  from './actions'
import LabelAndInput  from '../form/labelAndInput'
import LabelAndSelect from '../form/labelAndSelect'

const COMPONENT = {
    input : LabelAndInput,
    select: LabelAndSelect
}

class Form extends Component {
    render() {
        const { handleSubmit, readOnly, camposInserir, listRequest } = this.props
        return (
            <form role='form' onSubmit={handleSubmit}>
                 <div className='box-body'>
                    {camposInserir ?
                        camposInserir.map(campo=>{
                             return ( <div key={campo.id}>
                                <Field
                                     name     = {campo.name}
                                     id       = {campo.id}
                                     component= {COMPONENT[campo.component]}
                                     type     = {campo.type}
                                     readOnly = {readOnly}
                                     label    = {campo.label}
                                     cols     = {campo.cols}
                                     required = {campo.required}
                                     options  = {campo.options}
                                 />
                             </div>
                             )
                        }) : null
                    }
                </div>
                {
                    this.props.submitLabel ?
                        <div className='box-footer'>
                            <button type='submit' className={`btn btn-${this.props.submitClass}`}>
                                {this.props.submitLabel}
                            </button>
                            <button type='button' className='btn btn-default' onClick={()=>this.props.init(listRequest, sorting)}>Cancelar</button>
                        </div> :
                         <div className='box-footer'>
                            <button type='button' className='btn btn-default' onClick={()=>this.props.init(listRequest, sorting)}>Voltar</button>
                        </div>
                }
            </form>
        )
    }
}

Form                     = reduxForm({form: 'crudForm', destroyOnUnmount: false})(Form)
const selector           = formValueSelector('crudForm')
const mapStateToProps    = state    => ({crud: state.crud.list})
const mapDispatchToProps = dispatch => bindActionCreators({init, load}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Form)
