import React, { Component }   from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'

import {showUpdate, init} from './actions'

class Show extends Component {
    render() {
        const item    = this.props.values         || []
        const campos  = this.props.camposInserir  || []

        return (
            <div>
                <div className="box-body">
                    <div className="pull-left">
    			         <h4 >{item[campos[0].name.toLowerCase()] || ''}</h4>
    		         </div>
                    <div className="pull-right">
                        <button className='btn btn-sm btn-primary' onClick={() => this.props.showUpdate(item)}>
                            <i className='fa fa-pencil'></i>
                        </button>
        	        </div>
                    <br />
                    <hr className="default" />
                    <div className="col-md-3">
                        {
                            campos.map(campo=><div key={campo.id}>
                            <label>{campo.label}</label>: <span>{item[campo.name.toLowerCase()] || ''}</span>
                            <br />
                            </div>)
                        }
                        <br />
                    </div>
                </div>
                <div className='box-footer'>
                   <button type='button' className='btn btn-default' onClick={()=>this.props.init(this.props.listRequest, this.props.sorting)}>Voltar</button>
               </div>
            </div>
        )
    }
}

const mapStateToProps    = state    => ({values: state.form.crudForm.values})
const mapDispatchToProps = dispatch => bindActionCreators({showUpdate, init}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Show)
