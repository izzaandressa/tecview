import React, { Component }   from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'
import { getList }            from './actions'

import Loading from 'react-loading';
import Box     from '../template/box/box'

class BoxContainer extends Component {
    componentWillMount() {
        this.props.getList(this.props.listRequest,this.props.sorting)
    }

    render() {
        const {colunas, actions, colCards, list, sorting} = this.props
        return (
            <div>{
                list.length ? <ul>
                    {
                        list.map((item, key)=>(
                            <li key={`${key}-content-list`}>
                                <Box actions={actions     }
                                     conten ={colunas     }
                                     title  ={item.nome   }
                                     status ={item.status }
                                     col    ={colCards    }
                                     elemen ={item        }
                                     sorting={sorting     }
                                />
                            </li>
                         ))
                    }
                </ul> : <Loading  className="loading" type={'bars'} height={40} width={70} color='#222d32' />
            }</div>
    )}
}

const mapStateToProps    = state    => ({list: state.crud.list})
const mapDispatchToProps = dispatch => bindActionCreators({getList}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(BoxContainer)
