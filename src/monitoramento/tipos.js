import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS  = [
    {label:'Código',     name: 'codigo'     },
    {label:'Nome',       name: 'nome'       },
    {label:'Descrição',  name: 'descricao'  },
    {label:'Prioridade', name: 'prioridade' },
    {label:'Grupo',      name: 'nome_grupo' },
    {label:'Funções',    name: 'nome_pai'   }
]

const GRUPOOPT = [
    {label: '', value : ''},
    {label: 'teste', value : '2'},
    {label: 'tedste', value: '3'}
]

const PRIORIDADEOPT = [
    {label: '', value : ''},
    {label: 'Muito baixa', value: '0'},
    {label: 'Baixa',       value: '1'},
    {label: 'Média',       value: '2'},
    {label: 'Alta',        value: '3'},
    {label: 'Muito alta',  value: '4'},
]

const FUNCOESOOPT = [
    {label: '', value : ''},
    {label: 'teste', value : '2'},
    {label: 'tedste', value: '3'}
]

const INSERIR  = [
    {label: 'Código',     id: 'TipoOcorrenciaCodigoInput',     required: true,  name: 'codigo',     cols: "12", type: "txt", component: "input" },
    {label: 'Nome',       id: 'TipoOcorrenciaNomeInput',       required: true,  name: 'nome',       cols: "12", type: "txt", component: "input" },
    {label: 'Descrição',  id: 'TipoOcorrenciaDescricaoInput',  required: true,  name: 'descricao',  cols: "12", type: "txt", component: "input" },
    {label: 'Prioridade', id: 'TipoOcorrenciaPrioridadeInput', required: true,  name: 'prioridade', cols: "12", type: "txt", component: "select", options: PRIORIDADEOPT},
    {label: 'Grupo',      id: 'TipoOcorrenciaGrupoInput',      required: true,  name: 'grupo_id',   cols: "12", type: "txt", component: "select", options: GRUPOOPT},
    {label: 'Funções',    id: 'TipoOcorrenciaFuncaoInput',     required: false, name: 'funcao_id',  cols: "12", type: "txt", component: "select", options: FUNCOESOOPT},
]

class Tipos extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Tipo Ocorrência"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  sorting       = {true}
                  listRequest   ='tipos-ocorrencia'
                  createTipo    ='tipo-ocorrencia'
                  camposInserir ={INSERIR}
             />
        )
    }
}

export default Tipos;
