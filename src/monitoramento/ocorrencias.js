import React, { Component }   from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'

import ContentHeader from '../common/template/contentHeader'
import Content       from '../common/template/content'
import Tabs          from '../common/tab/tabs'
import TabsHeader    from '../common/tab/tabsHeader'
import TabsContent   from '../common/tab/tabsContent'
import TabHeader     from '../common/tab/tabHeader'
import TabContent    from '../common/tab/tabContent'
import Tabela        from '../common/crud/tabela'
import Show          from '../common/crud/show'

import {init,showCreate}        from '../common/crud//actions'

const COLUNAS  = [
    {label:'Código',       name: 'ocorrencia_id'   },
    {label:'Quando',       name: 'data_registro'   },
    {label:'Empresa',      name: 'empresa'         },
    {label:'Linha',        name: 'linha'           },
    {label:'Veículo',      name: 'veiculo'         },
    {label:'Tipo',         name: 'tipo_ocorrencia' },
    {label:'Infrator',     name: 'funcionario'     },
    {label:'Classificação',name: 'classificacao'   },
    {label:'Tratamento',   name: 'tratamento'      }
]

const INSERIR  = [
    {label:'Código',       name: 'ocorrencia_id'  , id: 'OcorreenciaCodigoInput',        required: true, cols: "12", type: "txt", component: "input"},
    {label:'Quando',       name: 'data_registro'  , id: 'OcorreenciaQuandoInput',        required: true, cols: "12", type: "txt", component: "input"},
    {label:'Empresa',      name: 'empresa'        , id: 'OcorreenciaEmpresaInput',       required: true, cols: "12", type: "txt", component: "input"},
    {label:'Linha',        name: 'linha'          , id: 'OcorreenciaLinhaInput',         required: true, cols: "12", type: "txt", component: "input"},
    {label:'Veículo',      name: 'veiculo'        , id: 'OcorreenciaVeiculoInput',       required: true, cols: "12", type: "txt", component: "input"},
    {label:'Tipo',         name: 'tipo_ocorrencia', id: 'OcorreenciaTipoInput',          required: true, cols: "12", type: "txt", component: "input"},
    {label:'Infrator',     name: 'funcionario'    , id: 'OcorreenciaInfratorInput',      required: true, cols: "12", type: "txt", component: "input"},
    {label:'Classificação',name: 'classificacao'  , id: 'OcorreenciaClassificacaoInput', required: true, cols: "12", type: "txt", component: "input"},
    {label:'Tratamento',   name: 'tratamento'     , id: 'OcorreenciaTratamentoInput',    required: true, cols: "12", type: "txt", component: "input"}
]

class Ocorrencia extends Component {
    componentWillMount() {
        this.props.init('ocorrencias/ocorrenciasByFiltro')
    }

    render() {
        return (
            <div>
                <ContentHeader title={this.props.title} small={this.props.small} />
                <Content>
                    <Tabs>
                        <TabsContent>
                            <TabContent id='tabList'>
                                <button type='button' className='btn btn-primary pull-right' onClick="">
                                    <i className="fa fa-filter"></i> Filtros
                                </button>
                                <Tabela key="tabela-ocorrencia" listRequest="ocorrencias/ocorrenciasByFiltro" colunas={COLUNAS} actions={[false,false]} />
                            </TabContent>
                            <TabContent id='tabShow'>
                                <Show camposInserir={INSERIR}/>
                            </TabContent>
                        </TabsContent>
                    </Tabs>
                </Content>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({init, showCreate}, dispatch)
export default connect(null, mapDispatchToProps)(Ocorrencia)
