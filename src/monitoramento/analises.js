import React, { Component }   from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'

import ContentHeader     from '../common/template/contentHeader'
import Content           from '../common/template/content'
import Tabs              from '../common/tab/tabs'
import TabsHeader        from '../common/tab/tabsHeader'
import TabsContent       from '../common/tab/tabsContent'
import TabHeader         from '../common/tab/tabHeader'
import TabContent        from '../common/tab/tabContent'
import Tabela            from '../common/crud/tabela'
import Show              from '../common/crud/show'

import AnaliseAberta    from './analise/analiseAberta'
import {init, toShowAnalise}  from './analise/actions'
//import {init, toShow}  from '../common/crud/actions'

const COLUNAS  = [
    {label:'Código',          name: 'id'          },
    {label:'Leiturista',      name: 'leiturista'  },
    {label:'Gravação',        name: 'data_imagem' },
    {label:'Abertura',        name: 'dataAbertura'},
    {label:'Ordem do Veículo',name: 'ordem'       },
    {label:'Duração',         name: 'leiturista'  },
    {label:'Status',          name: 'status'      }
]

const INSERIR  = [
    {label:'Código',    name: 'id',          id: 'AnaliseCódigoInput',     required: true, cols: "12", type: "txt", component: "input"},
    {label:'Leiturista',name: 'leiturista',  id: 'AnaliseLeituristaInput', required: true, cols: "12", type: "txt", component: "input"},
    {label:'Gravação',  name: 'data_imagem', id: 'AnaliseGravaçãoInput',   required: true, cols: "12", type: "txt", component: "input"},
    {label:'Abertura',  name: 'dataAbertura',id: 'AnaliseAberturaInput',   required: true, cols: "12", type: "txt", component: "input"},
    {label:'Ordem',     name: 'ordem',       id: 'AnaliseOrdemInput',      required: true, cols: "12", type: "txt", component: "input"},
    {label:'Duração',   name: 'leiturista',  id: 'AnaliseDuraçãoInput',    required: true, cols: "12", type: "txt", component: "input"},
    {label:'Status',    name: 'status',      id: 'AnaliseStatusInput',     required: true, cols: "12", type: "txt", component: "input"},
]

class Analise extends Component {
    componentWillMount() {
        this.props.init('analises/analisesByFiltro')
    }

    render() {
        return (
            <div>
                <ContentHeader title="Análise" small="Listagem" />
                <Content>
                    <Tabs>
                        <TabsContent>
                            <TabContent id='tabList'>
                                <button type='button' className='btn btn-primary pull-left' onClick={this.props.toShowAnalise}>
                                    <i className="fa fa-play"></i> Iniciar Análise
                                </button>
                                <button type='button' className='btn btn-primary pull-right' onClick="">
                                    <i className="fa fa-filter"></i> Filtros
                                </button>
                                <Tabela key="tabela-analise" listRequest="analises/analisesByFiltro"
                                    colunas={COLUNAS} actions={[false,false]}/>
                            </TabContent>
                            <TabContent id='tabShow'>
                                <Show camposInserir={INSERIR}/>
                            </TabContent>
                            <TabContent id='tabShowAnalise'>
                                <AnaliseAberta />
                            </TabContent>
                        </TabsContent>
                    </Tabs>
                </Content>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({init,toShowAnalise}, dispatch)
export default connect(null, mapDispatchToProps)(Analise)
