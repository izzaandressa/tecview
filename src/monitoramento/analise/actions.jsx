import axios                                             from 'axios'
import { toastr }                                        from 'react-redux-toastr'
import { reset as resetForm, initialize, initialValues } from 'redux-form'

import { showTabs, selectTab }  from '../../common/tab/tabActions'
import { getListRequest, submit }       from '../../common/request/crudRequest'

const INITIAL_VALUES = {}

function getList(tipo) {
    let data = {"sorting":{"nome":"asc"}}
    return {
        type   : 'ANALISE_FETCHED',
        payload: getListRequest(tipo, data)
    }
}

export function resetList() {
    return {
        type   : 'CRUD_FETCHED',
        payload: ''
    }
}

function analiseStatus(tipo) {
    return {
        type   : 'ANALISE_STATUS',
        payload: tipo
    }
}

export function toShowAnalise() {
    return [
        showTabs('tabShowAnalise'),
        selectTab('tabShowAnalise'),
        analiseStatus('aberta')
    ]
}

export function init(tipo) {
    return [
        resetList(),
        showTabs('tabList'),
        selectTab('tabList'),
        getList(tipo),
        analiseStatus('fechada')
    ]
}
