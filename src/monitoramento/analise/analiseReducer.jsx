const INITIAL_STATE = {list: []}

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case 'ANALISE_FETCHED':
            return { ...state, list: action.payload.data ? action.payload.data.data : [] }
        case 'ANALISE_STATUS':
            return { ...state, status: action.payload }
        default:
            return state
    }
}
