import React, { Component }   from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'

import {init} from './actions'

class AnaliseAberta extends Component {
    render() {

        return (
            <div>
                AINDA NÃO TEMOS O MULTIPLAYER =/
                <div className='box-footer'>
                   <button type='button' className='btn btn-default' onClick={()=>this.props.init('analises/analisesByFiltro')}>Voltar</button>
               </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({init}, dispatch)
export default connect(null, mapDispatchToProps)(AnaliseAberta)
