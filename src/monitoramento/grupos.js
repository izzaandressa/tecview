import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS  = [
    {label:'Nome',      name: 'nome'    },
    {label:'Grupo Pai', name: 'nome_pai'}
]

const GRUPOOPT = [
    {label: '', value : 0},
    {label: 'teste', value : 2},
    {label: 'tedste', value: 3},
]

const INSERIR  = [
    {label: 'Nome',                             id: 'GrupoNomeInput', required: true, name: 'nome',     cols: "12", type: "txt", component: "input" },
    {label: 'Pai (caso este seja um subgrupo)', id: 'GrupoPaiInput',  required: true, name: 'parent_id',cols: "12", type: "txt", component: "select", options: GRUPOOPT},
]

class Grupos extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Grupos"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  listRequest   ='grupos'
                  createTipo    ='grupo'
                  sorting       = {true}
                  camposInserir ={INSERIR}
            />
        )
    }
}

export default Grupos;
