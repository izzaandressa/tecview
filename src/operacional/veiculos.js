import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS = [
    {label:'Ordem',            name: 'ordem'    },
    {label:'Tipo',             name: 'nome_pai'},
    {label:'Placa',            name: 'placa'},
    {label:'Inicio Atividade', name: 'inicio_atividade'},
    {label:'Fim Atividade',    name: 'fim_atividade'},
    {label:'Empresa',          name: 'nome_empresa'},
    {label:'Fornecedor DVR',   name: 'fornecedor_dvr'},
    {label:'IP DEVR',          name: 'ip_dvr'},
    {label:'Observaçoes',      name: 'observacoes'}
]

const FUNCOESOOPT = [
    {label: '', value : ''},
    {label: 'teste', value : '2'},
    {label: 'tedste', value: '3'}
]

const INSERIR  = [
    {label: 'Ordem',                     id: 'VeiculoOrdemInput',              required: true,  name: 'ordem',            cols: "12", type: "txt", component: "input" },
    {label: 'Placa',                     id: 'VeiculoPlacaInput',              required: true,  name: 'placa',            cols: "12", type: "txt", component: "input"},
    {label: 'Tipo',                      id: 'VeiculoTipoInput',               required: true,  name: 'tipo_id',          cols: "12", type: "data",component: "select",  options: FUNCOESOOPT},
    {label: 'Empresa',                   id: 'VeiculoEmpresaInput',            required: true,  name: 'inicio_atividade', cols: "12", type: "data",component: "input"},
    {label: 'Fornecedor do DVR ',        id: 'VeiculoFornecedorDVR',           required: true,  name: 'fim_atividade',    cols: "12", type: "txt", component: "input"},
    {label: 'IP do DVR ',                id: 'VeiculoIdDvrInput',              required: true,  name: 'nome_empresa',     cols: "12", type: "txt", component: "input"},
    {label: 'Início atividade',          id: 'VeiculoInicioAtividadeInput',    required: true,  name: 'fornecedor_dvr',   cols: "12", type: "date",component: "input"},
    {label: 'Fim atividade',             id: 'VeiculoFimAtividadeInput',       required: false, name: 'ip_dvr',           cols: "12", type: "date",component: "input"},
    {label: 'Schema API de Itinerários', id: 'VeiculoSchemaApiItinerarioInput',required: false, name: 'observacoes',      cols: "12", type: "txt", component: "input"},
]

class Veiculos extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Veículo"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  listRequest   ='veiculos'
                  createTipo    ='veiculo'
                  camposInserir ={INSERIR}
                  sorting       = {false}
            />
        )
    }
}

export default Veiculos;
