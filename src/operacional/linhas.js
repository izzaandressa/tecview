import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS = [
    {label:'Codigo',    name: 'codigo'      },
    {label:'Descricao', name: 'descricao'   },
    {label:'Setor',     name: 'nome_setor'  },
    {label:'Empresa',   name: 'nome_empresa'}
]

const FUNCOESOOPT = [
    {label: '', value : ''},
    {label: 'teste', value : '2'},
    {label: 'tedste', value: '3'}
]

const INSERIR  = [
    {label: 'Código',           id: 'LinhaCodigoInput',         required: true,  name: 'codigo',          cols: "12", type: "txt", component: "input" },
    {label: 'Descrição',        id: 'LinhaDescricaoInput',      required: true,  name: 'descricao',       cols: "12", type: "txt", component: "input"},
    {label: 'Início atividade', id: 'linhaInicioAtividadeInput',required: true,  name: 'inicio_atividade',cols: "12", type: "date",component: "input"},
    {label: 'Fim atividade',    id: 'linhaFimAtividadeInput',   required: false, name: 'fim_atividade',   cols: "12", type: "date",component: "input"},
    {label: 'Setor',            id: 'LinhaSetorInput',          required: true,  name: 'setor_id',        cols: "12", type: "txt", component: "select", options: FUNCOESOOPT},
    {label: 'Empresa',          id: 'LinhaEmpresaInput',        required: true,  name: 'empresa_id',      cols: "12", type: "txt", component: "select", options: FUNCOESOOPT},
]

class Linhas extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Linha"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  listRequest   ='linhas'
                  createTipo    ='linha'
                  camposInserir ={INSERIR}
                  sorting       = {false}
            />
        )
    }
}

export default Linhas;
