import React, { Component } from 'react'

import CRUD from '../common/crud/crud'

const COLUNAS = [
    {label:'Nome', name: 'nome'}
]

const INSERIR  = [
    {label: 'Nome',id: 'SetorNomeInput',required: true,  name: 'nome', cols: "12", type: "txt", component: "input" },
]

class Setores extends Component {
    render() {
        return (
            <CRUD title         ="Cadastro de Setor"
                  small         ="Listagem"
                  colunas       = {COLUNAS}
                  listRequest   ='setores'
                  createTipo    ='setor'
                  sorting       = {true}
                  camposInserir ={INSERIR}
            />
        )
    }
}

export default Setores;
